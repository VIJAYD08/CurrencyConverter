package com.java.base;

import java.math.BigDecimal;

import com.java.model.ConverterModel;
import com.java.model.ResponseModel;
import com.java.util.Currency;


public interface ConverterBP {
	
	public ResponseModel getConvertedValue(Currency source, Currency target, BigDecimal amount);
	public ConverterModel getConverterModel();

}