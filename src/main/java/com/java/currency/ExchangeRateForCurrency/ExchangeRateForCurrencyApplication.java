package com.java.currency.ExchangeRateForCurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExchangeRateForCurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangeRateForCurrencyApplication.class, args);
	}
}
